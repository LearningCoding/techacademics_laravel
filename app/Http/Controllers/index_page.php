<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use App\register_details;
use Illuminate\Support\Facades\Hash;

class index_page extends Controller
{
    public function store(Request $request)
    {   
        $this->validate(request(),[
            'email' => 'required|email:rfc,dns',
        ]);
        $insert_data = new register_details();
        $request_data = request()->all();
        $insert_data->username = $request_data['username']; 
        $insert_data->email = $request_data['email']; 
        $insert_data->user_type = $request_data['user_type'];
        $insert_data->password = $request_data['password'];
        if($request->hasFile('image'))
        {
            $image = $request->file('image');
            $image_name =$image->getClientOriginalName();
            $image->move(public_path().'/profile_image/',$image_name);
        } else
        {
            $name = 'default.jpg';
        }
        $insert_data->image = $image_name;
        $insert_data->save();
        return redirect('instructor');
    }
    public function login_function(Request $request)
    {
        // $login_check_query = register_details::where('email',$request->email)->where('password',$request->password)->first();
        // if(!$login_check_query){
        //     // session()->flash('login_error','Your Credentials are not correct');
        //     return redirect('/');
        // }else
        // {
        //     Session::put('user_email',$login_check_query->email);
        //     return redirect('/');
        // }
        $login_check = register_details::where('email',$request->email)->first();

        // echo '<pre>';
        // print_r($login_check);

        // echo $login_check->password;

        if($login_check->password == $request->password){
            // echo '<script>alert("password matched");</script>';
            if($login_check->user_type == 'user')
            {
                Session::put('login_email',$login_check->email);
                Session::put('login_user_id',$login_check->id);
                Session::put('user_type',$login_check->user_type);
                return redirect('/');
            }else
            {
                Session::put('login_email',$login_check->email);
                Session::put('login_user_id',$login_check->id);
                Session::put('user_type',$login_check->user_type);
                return redirect('dashboard');
            }
        } else {
            
              session()->flash('login_error','Your Credentials are not correct');
              return redirect('/');
        }
        
    }

    public function show()
    {
        $get_user_details_query = register_details::all();
        // echo '<pre>';
        // print_r($get_user_details_query);
        return view('AdminModule/show_user')->with('register_details',$get_user_details_query);
    }

    public function check_email(Request $request)
    {
        // print_r($request->all());
        $check_email = register_details::where('email',$request->email)->exists();
        // return $check_title;
        if(!$check_email)
        {
            return 'new_email';
        }else
        {
            return 'email_exists';
        }
    }

    public function delete($id)
    {
        $delete_user_details_query = register_details::where('id',$id)->first();
        // echo '<pre>';
        // print_r($delete_user_details_query);
        $delete_user_details_query->delete();
        return redirect('showuser');
    }

    public function logout()
    {
        Session::flush();
        return redirect('/');
    }
}
