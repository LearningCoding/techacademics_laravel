<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\courses_data;
use App\add_field;
use Session;
class instructor_page extends Controller
{
    public function store(Request $request)
    {
        // echo '<pre>';
        // print_r($request->all());
        // die();
        $add_course = new courses_data();
        // $add_course->user_id = $request->user_id;
        // $add_course->user_email = $request->user_email;
        $add_course->name = $request->title;
        $add_course->category = $request->category;
        $add_course->course_specification = implode(',',$request->case_specification);
        $add_course->course_requirement = implode(',',$request->course_requirement);
        $add_course->course_content = implode(',',$request->course_content);
        $add_course->course_description = $request->course_description;
        $add_course->course_price = $request->course_price;
        if($request->hasFile('course_video'))
        {
            foreach($request->file('course_video') as $video_file)
            {
                $video_name = $video_file->getClientOriginalName();
                $video_file->move(public_path().'/instructor_course_videos/',$video_name);
                $video_array[] = $video_name;
            }
        }
        if($request->hasFile('course_image'))
        {
            foreach($request->file('course_image') as $image_file)
            {
                $image_name = $image_file->getClientOriginalName();
                $image_file->move(public_path().'/instructor_course_images/',$image_name);
                $image_array[] = $image_name;
            }
        }
        if($request->hasFile('course_notes'))
        {
            foreach($request->file('course_notes') as $notes_file)
            {
                $notes_name = $notes_file->getClientOriginalName();
                $notes_file->move(public_path().'/instructor_course_notes/',$notes_name);
                $notes_array[] = $notes_name;
            }
        }
        $add_course->image = implode(',',$image_array);
        $add_course->video = implode(',',$video_array);
        $add_course->document = implode(',',$notes_array);
        // print_r($add_course);
        $add_course->save();
        session()->flash('course_added','Your Course added Successfully');
        return redirect('dashboard');
        
        
    }
    public function course_category()
    {
        $get_all_courses = add_field::all();
        return view('InstructorModule.add_details')->with('courses_category',$get_all_courses);
    }

    public function check_title(Request $request)
    {
        // print_r($request->all());
        $check_title = courses_data::where('name',$request->title)->exists();
        // return $check_title;
        if(!$check_title)
        {
            return 'new_title';
        }else
        {
            return 'title_exists';
        }
    }

}
