<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\add_field;
class admin_page extends Controller
{
    public function store(Request $request)
    {
        // echo '<pre>';
        // print_r($request->all());
        $new_course_add_query = new add_field();
        $new_course_add_query->course_category = $request->course_category;
        $new_course_add_query->save();
        return redirect('admin');
    }
}
