<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

       // Routes for user
Route::get('/', function () {
    return view('StaticPages/main_page');
});

Route::post('details_send','index_page@store');

Route::post('login','index_page@login_function');

Route::get('logout','index_page@logout');

Route::get('contactus', function () {
    return view('StaticPages/contactus');
});
Route::get('aboutus', function () {
    return view('StaticPages/aboutus');
});
Route::get('instructor', function () {
    return view('StaticPages/instructor');
});
Route::get('cart', function () {
    return view('UserModule/cart_page');
});
Route::get('mycourses', function () {
    return view('UserModule/viewmycourses');
});
Route::get('uprofile', function () {
    return view('UserModule/profile');
});

Route::get('view', function () {
    return view('UserModule/viewmore');
});

Route::get('check_email','index_page@check_email');


// Routes for instructor

Route::get('dashboard', function () {
    return view('InstructorModule/instructordashboard');
});
Route::get('steps', function () {
    return view('InstructorModule/creationstep1');
});
Route::get('insprofile', function () {
    return view('InstructorModule/instructorprofile');
});
Route::get('details','instructor_page@course_category');

Route::get('showcourse', function () {
    return view('InstructorModule/show_course');
});

Route::post('create_course_form','instructor_page@store');

Route::get('check_title','instructor_page@check_title');


// Admin pages
Route::get('admin', function () {
    return view('AdminModule/admindashboard');
});

Route::get('addcat', function () {
    return view('AdminModule/add_course_category');
});

Route::post('add_course','admin_page@store');

Route::get('showuser','index_page@show');

Route::get('delete/{id}','index_page@delete');
