<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="{{asset('css/bootstrap.css')}}">
    <script src="{{asset('jquery-3.4.1.js')}}"></script>
    <script src="{{asset('js/bootstrap.js')}}"></script>
    <link rel="stylesheet" href="{{asset('fontawesome/css/all.css')}}">
    <link rel="stylesheet" href="{{asset('user_css/main.css')}}">
    <title>Home</title>
</head>
<body>
         <!--Menu Bar  -->
    @section('navbar')
    <div class="menu">
        <nav class="navbar navbar-expand-lg navbar-light" id="menubar">
            <a href="{{url('/')}}" class="navbar-brand text-white">Techacademics</a>
            <button class="navbar-toggler" data-toggle="collapse" data-target="#hit">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="hit">
                <ul class="navbar-nav mx-auto">
                    <li class="nav-item">
                        <a href="{{url('/')}}" class="nav-link">Home</a>
                    </li>
                    <li class="nav-item">
                        <div class="search-bar">
                            <form action=" ">
                            <input type="text" placeholder="Search">
                                <i class="fa fa-search search-icon"></i>
                            </form>
                        </div>
                    </li>
                    <li class="nav-item dropdown">
                        <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">Courses</a>
                        <div class="dropdown-menu">
                          <x-datashare/>
                        </div>
                    </li>
                    @if(Session::has('login_email'))
                    <li class="nav-item">
                      <a class="nav-link" href="{{url('mycourses')}}">My Courses</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="{{url('cart')}}">Cart</a>
                    </li>
                    <li class="nav-item dropdown profile">
                        <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown">
                          My Profile
                        </a>
                        <div class="dropdown-menu profile_dropdown">
                            <a class="dropdown-item" href="viewmycourses.html">My Courses</a>
                            <a class="dropdown-item" href="cart_page.html">My Cart</a>
                            <a class="dropdown-item" href="profile.html">Edit Profile</a>
                            <a class="dropdown-item" href="changepassword.html">Change Password</a>
                            <a class="dropdown-item" href="{{url('logout')}}">Logout</a>
                        </div>
                      </li>
                    @else
                    <li class="nav-item">
                        <a href="{{url('instructor')}}" class="nav-link">Instructor</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{url('contactus')}}" class="nav-link ">Contact Us</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{url('aboutus')}}" class="nav-link">About Us</a>
                    </li>
                </ul>
                <ul class="navbar-nav ml-auto login">
                    <li class="nav-item ">
                        <!-- Modal -->
<div class="modal fade" id="login" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">
  <div class="modal-dialog" role="document">

    <!--Content-->

    <div  class="modal-content  form">

      <!--Header-->

      <div class="modal-header text-center">
        <h3 class="modal-title w-100" id="myModalLabel"><strong>LogIn</strong></h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <!--Body-->

      <div class="modal-body mx-4">

        <!--Body-->
        <form action="{{url('login')}}" method="post">
        @csrf()
        <div class="text-primary md-form mb-5">
            <label data-error="required"  for="Form-email1">E-mail</label>

            <input type="email" id="Form-email1"  class="form-control validate bg-transparent" name="email" required>
        </div>

        <div class=" text-primary md-form pb-3">
            <label data-error="required"  for="Form-pass1"> Password</label>

            <input type="password" id="Form-pass1" class="form-control validate  bg-transparent" name="password" required>
          <p class="font-small text-dark d-flex justify-content-end">Forgot <a href="#" class="blue-text ml-1">
              Password?</a></p>
        </div>

        <div class="text-center mb-3">
          <button type="submit" class="btn blue-gradient btn-block btn-rounded z-depth-1" name="login">LogIn</button>
        </div>
        <p class="font-small dark-grey-text text-right d-flex justify-content-center mb-3 pt-2"> or LogIn
          with:</p>

          <!--icon-->
        <div class="row my-3 d-flex justify-content-center">
          
          <button type="button" class="btn btn-primary btn-rounded w-25 mr-md-3 z-depth-1"><i class="fab fa-facebook-f text-center"></i></button>
          
          <button type="button" class="btn btn-info btn-rounded w-25 ml-1 mr-md-3 z-depth-1"><i class="fab fa-twitter"></i></button>
          
          <button type="button" class="btn btn-danger btn-rounded w-25 ml-1 z-depth-1"><i class="fab fa-google-plus-g"></i></button>
        </div>
        </form>

      </div>
      <!--Footer-->
      <div class="modal-footer mx-5 pt-3 mb-1">
        <p class="font-small grey-text d-flex justify-content-end">Not a member? <a href="http://localhost/learning%20website%20PHP/registration.php" class="blue-text ml-1">
            Sign Up</a></p>
      </div>
    </div>
    <!--Content-->
  </div>
</div>
<!-- Modal -->
<div class="text-center">
  <a href="" class="btn btn-default btn-rounded" data-toggle="modal" data-target="#login">
     Login </a>
</div>    
                    </li>
                    <li class="nav-item ">
                        <!-- Modal -->
<div class="modal fade" id="register" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">

    <!--Content-->

    <div class="modal-content  form">

      <!--Header-->

      <div class="modal-header text-center">
      @if($errors->any())
          <div class="alert alert-danger">
              <ul class="list-group">
                  @foreach($errors->all() as $error)
                  <li class="list-group-item">{{$error}}</li>
                  @endforeach
              </ul>
          </div>
          @endif
        <h3 class="modal-title w-100" id="myModalLabel"><strong>SignIn</strong></h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <!--Body-->

      <div class="modal-body mx-4">

        <!--Body-->
        <form action="{{url('details_send')}}" method="post" enctype='multipart/form-data'>
        @csrf()
        <div class="text-primary md-form mb-3">
            <label data-error="required"  for="name">Name</label>

            <input type="text" id="name" name="username" class="form-control validate bg-transparent"required>
        </div>

        <div class="text-primary md-form mb-3">
            <label data-error="required"  for="Form-email1" name="email">E-mail</label>

            <input type="email" id="get_email" name="email" class="form-control validate bg-transparent"required pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$">
            <div class="email_error h4"></div>
        </div>
        <div class=" text-primary md-form pb-3">
            <label data-error="required"  for="Form-pass1"> User Type</label>
                <select name="user_type" id="" class='form-control' required>
                    <option value="user">User</option>
                    <option value="instructor">Instructor</option>
                </select>
        </div>
        <div class=" text-primary md-form pb-3">
            <label data-error="required"  for="Form-pass1"> Profile image</label>
            <input type="file" id="Form-pass1" name="image" class="form-control validate  bg-transparent" required>
        </div>
        <div class=" text-primary md-form pb-3">
            <label data-error="required"  for="Form-pass1" name="password"> Password</label>

            <input type="password" id="Form-pass1" name="password" class="form-control validate  bg-transparent"required>
          <p class="font-small text-dark d-flex justify-content-end">Forgot <a href="#" class="blue-text ml-1">
              Password?</a></p>
        </div>

        <div class="text-center mb-3">
          <input type="submit" class="btn blue-gradient btn-block btn-rounded z-depth-1" name="submit" value="Register" id="signin_bt">
        </div>
        <p class="font-small dark-grey-text text-right d-flex justify-content-center mb-3 pt-2"> or SignIn
          with:</p>

          <!--icon-->
        <div class="row my-3 d-flex justify-content-center">
          
          <button type="button" class="btn btn-primary btn-rounded w-25 mr-md-3 z-depth-1"><i class="fab fa-facebook-f text-center"></i></button>
          
          <button type="button" class="btn btn-info btn-rounded w-25 ml-1 mr-md-3 z-depth-1"><i class="fab fa-twitter"></i></button>
          
          <button type="button" class="btn btn-danger btn-rounded w-25 ml-1 z-depth-1"><i class="fab fa-google-plus-g"></i></button>
        </div>
        </form>
      </div>
    </div>
    <!--Content-->
  </div>
</div>
<!-- Modal -->
<div class="text-center">
  <a href="" class="btn btn-default btn-rounded" data-toggle="modal" data-target="#register">
     Signin</a>
</div>   
                    </li>
                    @endif
                </ul>
            </div>
        </nav>
    </div>
    @show


    @yield('main_content')

    <!-- Footer -->
    @section('footer')
    <footer class="footer container-fluid">
        <div class="upper-footer">
<div class="row">
                <div class="col-md-3 mx-auto mt-4">
                    <p>Our mission is to provide a free, world-class education to anyone,anywhere.</p>
                    <!-- <img src="bby.jpg " alt=" " class="img-fluid d-block img mb-2 "> -->

                </div>
                <div class="col-md-3 mx-auto mt-4">
                <p class="font-weight-bold">ABOUT US</p>
                    <p><a>News</a></p>
                    <p><a>Impact</a></p>
                    <p><a>Our team</a></p>
                    <p><a>Our leadership</a></p>
                    <p><a>Careers</a></p>
                    <p><a>Internship</a></p>
                </div>
                <div class="col-md-3 mt-4">
                    <p class="font-weight-bold">CONTACT US</p>
                    <p><span class="font-weight-bold">Address :</span> Hath of it fly signs bear be one blessed after</p>
                    <p><a>Help center</a></p>
                    <p><a>Share your story</a></p>

                    <p><a><span class="font-weight-bold">Phone :</span> 600540987</a></p>
                    <p><a><span class="font-weight-bold">Email :</span>info@learning.com </a></p>
                </div>
            </div>
        </div>
        <hr>
        <div class="icons">
            <div class="row">
                <div class="col-md-6 list mx-auto">
                    <ul>
                        <li>Terms of use</li>
                        <li class="ml-5">Privacy Policy</li>
                    </ul>
                </div>
                <div class="col-md-6">
                    <i class="fab fa-facebook"></i>
                    <i class="fab fa-twitter"></i>
                    <i class="fab fa-instagram"></i>
                </div>
            </div>
        </div>
        <hr>
        <div class="lower-footer text-center">
            Copyright @E-Learning All rights reserved
        </div>
    </footer>
    @show

    <script src="https://code.jquery.com/jquery-1.9.1.js"></script>
    <script>
    $(function(){
        $('#get_email').keyup(function(){
            let email = $(this).val();
            $.ajax({
                url:'{{url("check_email")}}',
                type:'get',
                data:{email,email},
                success:function(data)
                {
                    if(data == 'email_exists')
                    {
                        $('#get_email').addClass('border').addClass('border-danger');
                        $('.email_error').css('display','block');
                        $('.email_error').removeClass('bg-success').addClass('bg-danger').addClass('text-white');
                        $('.email_error').html('Account already exists with this email');
                        $(':input[type="submit"]').prop('disabled', true);
                    }else
                    {
                        $('.email_error').removeClass('bg-danger').addClass('bg-success').addClass('text-white');
                        $('.email_error').css('display','block');
                        $('.email_error').html('You can use this email');
                        $('#get_email').addClass('border').addClass('border-success');
                        $(':input[type="submit"]').prop('disabled', false);
                    }
                }
            })
        })
    })
    </script>


</body>
</html>