<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Registered Users</title>
    <link rel="stylesheet" href="{{asset('css/bootstrap.css')}}">
    <script src="{{asset('jquery-3.4.1.js')}}"></script>
    <script src="{{asset('js/bootstrap.js')}}"></script>
    <link rel="stylesheet" href="{{asset('fontawesome/css/all.css')}}">
</head>
<body>
<div class="container">
    <div class="row">
    <div class="col-md-12">
    @if(Session::has('login_email'))
    {{Session::get('login_email')}}
    @endif
    <table class="table table-bordered">
    <thead class="thead-dark">
    <tr>
    <th>User Id</th>
    <th>Username</th>
    <th>Email</th>
    <th>User Type</th>
    <th>Action</th>
    </tr>
    </thead>
    <tbody>
        @foreach($register_details as $data)
            <tr>
            <td>{{$data->id}}</td>
            <td>{{$data->username}}</td>
            <td>{{$data->email}}</td>
            <td>{{$data->user_type}}</td>
            <td>
            <a href="{{url('delete',[$data->id])}}"><button class="btn btn-danger">Delete</button></a>
            </td>
            </tr>

        @endforeach
    </tbody>
    </table>
    </div>
    </div>
</div>
</body>
</html>