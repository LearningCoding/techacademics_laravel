<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Add category</title>
    <link rel="stylesheet" href="{{asset('css/bootstrap.css')}}">
    <script src="{{asset('jquery-3.4.1.js')}}"></script>
    <script src="{{asset('js/bootstrap.js')}}"></script>
    <link rel="stylesheet" href="{{asset('fontawesome/css/all.css')}}">
</head>
<body>
    
    <div class="container bg-light">
        <div class="row">
            <div class="col-md-6 bg-info mx-auto text-white" >
                <h2 class="text-center">
                    Add Course Category
                </h2>
                <form action="{{url('add_course')}}" method="post" enctype= 'multipart/form-data'>
                    @csrf()
                    <label for="">Course Category</label>
                    <input type="text" name='course_category' class='form-control'>
                    <input type="submit" value='Add category' class="btn btn-danger mt-3">
                </form>
            </div>
        </div>
    </div>

</body>
</html>