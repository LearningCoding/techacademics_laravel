<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Home Page</title>
    <link rel="stylesheet" href="{{asset('css/bootstrap.css')}}">
    <script src="{{asset('jquery-3.4.1.js')}}"></script>
    <script src="{{asset('js/bootstrap.js')}}"></script>
    <link rel="stylesheet" href="{{asset('fontawesome/css/all.css')}}">
    <link rel="stylesheet" href="{{asset('admin_css/admindashboard.css')}}">
</head>

<body>
     <!--section1-->
     <nav class="navbar navbar-expand-xl bg-dark navbar-dark">
        <!-- Toggler/collapsibe Button -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
          <span class="navbar-toggler-icon"></span>
        </button>
      
        <!-- Navbar links -->
        <div class="collapse navbar-collapse" id="collapsibleNavbar">
          <ul class="navbar-nav">
            <li class="nav-item">
              <a class="nav-link" href="#">View Students</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">View Instructors</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">Add features</a>
            </li>
          </ul>
        </div>
      </nav>
      <!--section2-->
      <div class="container-fluid">
          <div class="row mt-4 row2">
              <div class="col-md-12 col-lg-12">
                  <h3 class="mt-3">Welcome Admin User ,</h3>
              </div>
          </div>
            <!--section3-->
            <div class="row text-white text-center mt-4">
                <div class="col-md-4 col-lg-4 ">
                    <div class="col3">
                    <i class="fas fa-user icon3"></i>
                    <h4>Total Admin</h4>
                </div>
                </div>
                <div class="col-md-4 col-lg-4 ">
                    <div class="col3">
                        <i class="fas fa-user icon4"></i>
                    <h4>Total Teachers</h4>
                </div>
                </div>
                <div class="col-md-4 col-lg-4 ">
                    <div class="col3">
                        <i class="fas fa-user icon5"></i>
                    <h4>Total Students</h4>
                </div>
                </div>
            </div>
      </div>
</body>
</html>