@extends('Navbar.navbar_footermain')

@section('navbar')

@parent

@endsection


@section('main_content')
<link rel="stylesheet" href="{{asset('user_css/aboutus.css')}}">
<div class="aboutus-content">
    <div class="container">
        <div class="col-md-12 bg-white mx-auto text-center ">
             <h1> Awesome Feature</h1>
             <h2 class="content-c">The leading global marketplace for learning and instruction</h2>
            <p class="content-p1 mt-3">By connecting students all over the world to the best instructors, Tech-Acedmics is helping individuals reach their goals and pursue their dreams.
Talent is universal, but opportunities are not. With access to online learning resources and instruction, anyone, anywhere, can gain skills and transform their lives in meaningful ways.

            </p>
                <a href="#" class="btn1" style="text-decoration:none;">Read More</a>
        </div>
    </div>
</div>    


<div class="content">
    <div class="container">
        <div class="row mx-auto mt-5">
            
            <div class="col-md-3  mx-auto text-center" >
                <i class="far fa-clone  mt-5 d-block i5"></i>
                      <h4  style="margin-bottom: 20px;margin-top:20px;" >Better Future</h4>
                        <p>Tech-Acedmics believes the world’s best teachers aren’t always found in classrooms. Our instructors come from virtually every country and offer courses in 65+ languages on practically any topic.</p>
               </div>
            
            <div class="col-md-3 mx-auto text-center">
                <i class="fas fa-book-reader mt-5 d-block i5"></i>
                <h4 style="margin-bottom: 20px;margin-top:20px;" >Qualified Trainers</h4>
                <p> Tech-Acedmics instructors are passionate about sharing their knowledge and helping students. They’re experts who stay active in their fields in order to deliver the most up-to-date content.</p>
             </div>
           
             <div class="col-md-3 mx-auto text-center ">
                <i class="far fa-bell   mt-5 d-block i5"></i>
                <h4  style="margin-bottom: 20px;margin-top:20px;" >Job Oppurtunity</h4>
                <p >Tech-Acedmics helps organizations of all kinds prepare for the ever-evolving future of work. Our curated collection of top-rated business and technical courses gives companies, governments.</p>
            </div>
        </div>
    </div>   
</div>


    <div class="staff-content mt-5">
    <div class="container-fluid con-fu">
        <div class="row no-pad">
            <div class="col-md-6">
                <div class="staff-content-1">
         <div class="staff-title">
                    <p class="p_box__title">Staying ahead of the future of work
            </p>
        </div>

              <div class="staff-left-content">
            <p class="p_box__copy">Udemy helps organizations of all kinds prepare for the ever-evolving future of work. Our curated collection of top-rated business and technical courses gives companies, <a href="https://government.udemy.com/" target="_blank">governments</a>, and nonprofits the power to develop in-house expertise and satisfy employees’ hunger for learning and development.</p>
                </div>
</div>
</div>
            


            <div class="col-md-6">
                <div class="staff-img">
                    <img src="Images/video-still-pinterest.jpg">
                </div>
            </div>

    </div>



<div class="row no-pad">
            <div class="col-md-6">
                <div class="staff-img-1">
                    <img src="Images/video-still-ufb.jpg">
                </div>
            </div>

            <div class="col-md-6">
                <div class="staff-content-2">
<div class="staff-title-1">
                    <p class="p_box__title-1">Staying ahead of the future of work
            </p>
</div>
<div class="staff-title-contemt-1">
            <p class="p_box__copy-1">Udemy helps organizations of all kinds prepare for the ever-evolving future of work. Our curated collection of top-rated business and technical courses gives companies, <a href="https://government.udemy.com/" target="_blank">governments</a>, and nonprofits the power to develop in-house expertise and satisfy employees’ hunger for learning and development.</p>
               </div>
                 </div>
            </div>
        </div>
    </div>
</div>

<!-------------end--------------->

        <div class="container-fluid">
            <div class="row mx-auto mt-5 ">
                <div class="col-md-6 mx-auto">
                <h1 style="text-align: center;">Free tools for parents and teachers</h1>
                <p style="text-align: center;">We’re working hard to ensure that Khan Academy empowers coaches of all kinds to better
                understand what their children or students are up to and how best to help them. See at a glance whether
                a child or student is struggling or if she hit a streak and is now far ahead of the class. 
                Our teacher dashboard provides a summary of class performance as a whole as well as detailed student profiles.</p>
            </div>
        </div>
        </div>



        <div class="jion-team">
            <div class="container">
                <div class="row mx-auto mt-5 no-pad mb-5">
                    <div class="col-md-6">
                        <div class="left-content">
                            <p class="section-title">A great place to grow</p>
                        
                            <p  class="section-content  mt-5">Tech-acedmics employees live out our <u><a href="/ideas-and-opinions/who-we-are-at-udemy/">values</a></u> every day as learners and teachers ourselves. Our culture is diverse, inclusive, and committed to personal and professional development. We’re not afraid to take on a new challenge, and we love taking Udemy courses!</p>
                            <a href="#" class="button-team" style="text-decoration: none;">Join our team</a>
                        </div>
                    </div>
                    <div class="col-md-6 ">
                        
                        <div class="top-img">
                            <img src="Images/grow-1-572x311.jpg">
                        </div>

                        <div class="row no-pad">
                             <div class="col-md-6 ">
                        <div class="right-img">
                            <img src="Images/grow-2.jpg">
                        </div>
                    </div>
                     <div class="col-md-6 ">
                        <div class="left-img">

                            <img src="Images/grow-3.jpg">
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<!------------nf-------------------->
@endsection

@section('footer')

@parent

@endsection