@extends('Navbar.navbar_footermain')

@section('navbar')

@parent

@endsection


@section('main_content')

<!-- Main Image -->
<div class="image">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <p>EVERY CHILD YEARNS TO LEARN</p>
                <h1>Making Your Childs World Better</h1>
                <p>Replenish seasons may male hath fruit beast were seas saw you arrie said man beast whales his void unto last session for bite. Set have great you'll male grass yielding yielding man</p>
                <input type="button" value="View Course" class="btn bg-light bt-1">
                <input type="button" value="Get Started" class="btn bg-light bt-2">
            </div>
        </div>
    </div>
</div>
<!-- First Section / Learning Div -->
<div class="first-section container-fluid">
    <div class="row mt-4">
        <div class="col-md-6">
            <img src="../Images/learning.webp" alt="" class="img-fluid">
        </div>
        <div class="col-md-6">
            <h1>Learning With Love and Laughter</h1>
            <p>No matter how busy you may think you are, you must find time for reading, or surrender yourself to self-chosen ignorance.The art of reading and studying consists in remembering the essentials nd forgetting what is not essential.To
                acquire knowledge,one must study;but to acquire wisdom,one must observe.
            </p>
            <p><i class="fas fa-pen"></i> Him lights given i heaven second yielding seas gathered wear </p>
            <p><i class="fas fa-book-open"></i> Fly female them whales fly them day deep given night</p>
        </div>
    </div>
</div>
<!-- Second Section / Choose us content -->
<div class="content">
    <h1 class="text-center">Why to choose us ?</h1>
    <div class="container">
        <div class="row mt-5">
            <div class="col-md-4 mx-auto text-center">
                <div class="card">
                    <img src="Images/svgimg1.png" alt="" class="card-img-top d-block mx-auto mt-5">
                    <div class="card-body">
                        <h4 class="card-title">Personalized learning</h4>
                        <p class="card-text">Students practice at their own pace, first filling in gaps in their understanding and then accelerating their learning.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 mx-auto text-center">
                <div class="card">
                    <img src="Images/svgimg3.png" alt="" class="card-img-top d-block mx-auto mt-5">
                    <div class="card-body">
                        <h4 class="card-title">Trusted content</h4>
                        <p class="card-text">Created by experts, E-Learning library of trusted, standards-aligned practice and lessons covers It’s all free for learners.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 mx-auto text-center">
                <div class="card">
                    <img src="Images/svgimg2.png" alt="" class="card-img-top d-block mx-auto mt-5">
                    <div class="card-body">
                        <h4 class="card-title">Tools to empower teachers</h4>
                        <p class="card-text">With E-Leaning, teachers can identify gaps in their students’ understanding, tailor instruction, and meet the needs of every.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Third Section / Slider -->
<div class="slider">
    <div class="container-fluid p-0">
        <div class="carousel slide" data-ride="carousel" id="sliderMove">
            <ol class="carousel-indicators">
                <li data-target="#sliderMove" data-slide-to="0" class="active"></li>
                <li data-target="#sliderMove" data-slide-to="1"></li>
                <li data-target="#sliderMove" data-slide-to="2"></li>
            </ol>

            <!-- Carousel item -->
            <div class="carousel-inner">
                <div class="carousel-item active" style="background-image: url('../Images/slider1.jpeg');">
                    <div class="carousel-caption">
                        <h1>Don’t let what you cannot do interfere with what you can do.</h1>
                    </div>
                </div>
                <div class="carousel-item" style="background-image: url('../Images/slider2.jpeg');">
                    <div class="carousel-caption">
                        <h1>Strive for progress, not perfection.</h1>
                    </div>
                </div>
                <div class="carousel-item" style="background-image: url('../Images/slider3.jpeg');">
                    <div class="carousel-caption">
                        <h1>Failure is the opportunity to begin again more intelligently.</h1>
                    </div>
                </div>
            </div>

            <!-- Slider Icon -->
            <a class="carousel-control-prev" href="#sliderMove" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon"></span>
            </a>
            <a class="carousel-control-next" href="#sliderMove" role="button" data-slide="next">
                <span class="carousel-control-next-icon"></span>
            </a>
        </div>
    </div>
</div>
<!-- Fourth Section / Top Categories -->
<div class="fourth-section container">
    <h1 class="text-center mb-5">Top Categories</h1>
    <div class="row">
        <div class="col-md-4 mx-auto mb-5">
            <h3 class="text-center mb-5">Buisness</h3>
            <div class="card">
                <img src="../Images/buisness.jpg" alt="" class="card-img-top">
                <div class="layer">
                    <button class="btn btn-info" onclick="window.location.replace('viewmore.html')">View More</button>
                </div>
                <div class="card-body">
                    <h5 class="card-title">An entire MBA in 1 Course</h5>
                </div>
            </div>
        </div>
        <div class="col-md-4 mx-auto mb-5">
            <h3 class="text-center mb-5">IT</h3>
            <div class="card">
                <img src="../Images/it.jpg" alt="" class="card-img-top">
                <div class="layer2">
                    <button class="btn btn-info">View More</button>
                </div>
                <div class="card-body">
                    <h5 class="card-title">AWS Certified Sloutions</h5>
                </div>
            </div>
        </div>
        <div class="col-md-4 mx-auto mb-5">
            <h3 class="text-center mb-5">Marketing</h3>
            <div class="card">
                <img src="../Images/marketing.jpg" alt="" class="card-img-top">
                <div class="layer">
                    <button class="btn btn-info">View More</button>
                </div>
                <div class="card-body">
                    <h5 class="card-title">Digital Marketing Course</h5>
                </div>
            </div>
        </div>
        <div class="col-md-4 mx-auto">
            <h3 class="text-center mb-5">Development</h3>
            <div class="card">
                <img src="../Images/development.jpg" alt="" class="card-img-top">
                <div class="layer2">
                    <button class="btn btn-info">View More</button>
                </div>
                <div class="card-body">
                    <h5 class="card-title">Python Complete Bootcamp</h5>
                </div>
            </div>
        </div>
        <div class="col-md-4 mx-auto">
            <h3 class="text-center mb-5">Heath & Fitness</h3>
            <div class="card">
                <img src="../Images/health.jpg" alt="" class="card-img-top">
                <div class="layer">
                    <button class="btn btn-info">View More</button>
                </div>
                <div class="card-body">
                    <h5 class="card-title">Cognitive Behavioural Therapy</h5>
                </div>
            </div>
        </div>
        <div class="col-md-4 mx-auto">
            <h3 class="text-center mb-5">Photography</h3>
            <div class="card">
                <img src="../Images/photography.jpg" alt="" class="card-img-top">
                <div class="layer2">
                    <button class="btn btn-info">View More</button>
                </div>
                <div class="card-body">
                    <h5 class="card-title">Photography Masterclass</h5>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Fifth Section / Instructor - Buisness -->
<div class="buisness container-fluid">
    <div class="row">
        <div class="col-md-4 mx-auto text-center">
            <h4>Become an instructor</h4>
            <p>Top instructors from around the world teach millions of students on E-Learning. We provide the tools and skills to teach what you love. </p>
            <button class="btn btn-secondary">Start Teaching Today</button>
        </div>
        <div class="col-md-4 mx-auto text-center">
            <h4>E-Learning for Buisness</h4>
            <p>Get unlimited access to 4,000+ of E-Learning’s top courses for your team.</p>
            <button class="btn btn-secondary">Get E-Learning for Buisness</button>
        </div>
    </div>
</div>
<!-- Sixth Section- Image/Content -->
<div class="sixth-section container-fluid">
    <div class="row">
        <div class="col-md-5 mx-auto text-center sixthsec-content">
            <p>87% of people learning for professional development report career benefits like getting a promotion, a raise, or starting a new career</p>
            <button class="btn btn-info">Join For Free</button>
        </div>
        <div class="col-md-6">
            <img src="../Images/people.png" alt="" class="img-fluid sixthsec-img">
        </div>
    </div>
</div>
<!-- Seventh Div / Student Blog -->
<div class="seventh-section container-fluid">

</div>

<!-- Javascript -->
<script src="../Javascript/index.js"></script>

@endsection

@section('footer')

@parent

@endsection
