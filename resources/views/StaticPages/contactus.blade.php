@extends('Navbar.navbar_footermain')

@section('navbar')

@parent

@endsection


@section('main_content')
<link rel="stylesheet" href="{{asset('user_css/contact_us.css')}}">
    <!-- Main Image -->

    <div class="inner-image container p-0">
        <!-- <h1>Contact Us</h1> -->
    </div>
    <!-- div for map-->
    <div id="body">
        <div class="container" id="inner_body">
            <div class="row">
                <div class="col-md-6 " id="map">
                    <h2>SachTech Solution Pvt. Ltd</h2>
                    <iframe id="iframe" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13720.225131641044!2d76.6946658!3d30.7168181!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xb713d3a3e26a89c1!2sSachTech%20Solution%20Pvt.%20Ltd.!5e0!3m2!1sen!2sin!4v1581603434671!5m2!1sen!2sin"
                        width="450" height="450" frameborder="0" style="border: 2px solid darkgray;" allowfullscreen=""></iframe>
                    <div id="infor">
                        <h5 id="head">STUDENT CONTACT CENTER</h5>
                        <p id="para">+91 9241333666</p>
                        <p id="para">support@byjus.com</p>
                    </div>
                    <div id="infor">
                        <h5 id="head">FOR ANY MEDIA ENQUIRY</h5>
                        <p id="para">press@byjus.com</p>
                    </div>
                    <div id="infor">
                        <h5 id="head">FOR ANY BUSINESS ENQUIRY</h5>
                        <p id="para">contact@byjus.com</p>
                    </div>
                    <div id="infor">
                        <h5 id="head">UAE ENQUIRY</h5>
                        <p id="para">byjus@moreideas.ae</p>
                    </div>

                </div>
                <div class="col-md-6" id="get">
                    <h2 class="contact-title">Get in Touch</h2>
                    <div class="form-group">
                        <textarea class="form-control " name="message" cols="50" rows="9" placeholder="Enter Message" id="message"></textarea>
                    </div>
                    <div class="form-group">
                        <input input type="text" class="form-control" name="name" id="name" placeholder="Enter your name ">
                    </div>
                    <div class="form-group">
                        <input input type="text" class="form-control" name="email" id="email" placeholder="Enter your email ">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" name="subject" id="subject" placeholder="Enter SUBJECT">
                    </div>
                    <div>
                        <button class="btn btn-info">SEND MESSAGE</button>
                    </div>
                    <div id="infor">
                        <h5 id="head">HEADQUARTERS</h5>
                        <p id="para">BYJU'S, 2nd Floor, Tower D, IBC Knowledge Park, 4/1, Bannerghatta MainRoad, Bengaluru</p>
                        <p id="para">+91 9880031619</p>
                    </div>
                    <div id="infor">
                        <h5 id="head">Complaint Resolution</h5>
                    </div>
                    <div id="infor">
                        <h5 id="head">Customer Care</h5>
                    </div>
                    <div id="infor">
                        <h5 id="head">Support Center</h5>
                    </div>

                </div>
            </div>
        </div>
@endsection

@section('footer')

@parent

@endsection