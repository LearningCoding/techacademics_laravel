@extends('Navbar.navbar_footermain')

@section('navbar')

@parent

@endsection


@section('main_content')
<link rel="stylesheet" href="{{asset('user_css/viewmore.css')}}">
    <!--section2-->
    <div class="container-fluid">
        <div class="row row1 text-white">
            <div class="col-md-6 ml-5 sec1">
                 <span class="font-weight-bold h5">Development</span>
                <span class="ml-3">></span>
                <span class="ml-3">Web Development</span>
                <span class="ml-3">></span>
                <span class="ml-3">Node.js</span>
            </div>
            <div class="col-md-8 col1 mx-auto mt-2 mb-2">
                <h3>Node.js API Masterclass With Express & <br>MongoDB</h3>
                <p class="h6 font-weight-normal">Create a realworld backend for bootcamp directory</p>
                <p class="h6 font-weight-normal"><span class="badge badge-dark badge-pill">HIGHEST RATED</span>
                    <i class="fas fa-star icon ml-2"></i><i class="fas fa-star icon ml-2"></i><i class="fas fa-star icon ml-2"></i>
                    <i class="fas fa-star icon ml-2"></i><i class="fas fa-star icon ml-2"></i> 6,939 students enrolled</p>
                <p>Created by BRAD TRAVERSY</p>
                <p><i class="fas fa-cog mr-2"></i>Last updated 7/2020</p>
                <p><i class="fas fa-globe mr-2"></i>English</p>
            </div>
        </div>
    </div>
        <!--section3-->
        <div class="container mt-2 mb-4">
            <div class="row">
                <div class="col-md-8 col2">
                    <p class="h5 mt-2 mb-2">What You,ll Learn</p>
                    <p><i class="fas fa-check mr-2 mt-3 h6"></i>Real world backend RESTful API for bootcamp Directory App</p>
                    <p><i class="fas fa-check mr-2 h6"></i>Advanced Mongoose Queries</p>
                    <p><i class="fas fa-check mr-2 h6"></i>Expressed & Mongoose Middleware (Geocoding,Auth,Error Handling, etc)</p>
                    <p><i class="fas fa-check mr-2 h6"></i> API Documentation & Deployment</p>
                    <p><i class="fas fa-check mr-2 h6"></i> HTTP Fundamentals(Req/Res Cycle,Status Codes,etc)</p>
                    <p><i class="fas fa-check mr-2 h6"></i>JWT/Cookie Authentication</p>
                    <p><i class="fas fa-check mr-2 h6"></i> API Security</p>

                </div>
                <div class="col-md-4 col-lg-4 col3 mx-auto">
                    <div class="card">
                        <iframe width="288" height="200" src="https://www.youtube.com/embed/5jQSat1cKMo" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        <div class="card-body">
                            <h4 class="card-title"><i class="fas fa-rupee-sign"></i>420</h4>
                            <p class="card-text">
                                <button class="btn btn1 form-control mt-2">ADD TO CART</button>
                                <button class="btn btn2 btn-outline-dark form-control mt-2">BUY NOW</button>
                            </p>
                            <p class="mt-2 text-center">30-Day Money Back Guarantee</p>
                            <p class="h5 mt-3">This course includes</p>
                            <p class="mt-2"><i class="fas fa-file-video mr-2"></i>12 hours on demand video</p>
                            <p><i class="fas fa-circle mr-2"></i>Full lifetime access</p>
                            <p><i class="fas fa-mobile-alt mr-2"></i>Access on mobile</p>
                            <p><i class="fas fa-certificate mr-2"></i>Certificate of completion</p>
                            <hr>
                            <a href=""><i class="fas fa-share mr-2 icon1"></i>Share</a>
                            <hr>
                        </div>
                    </div>
                </div>
                </div>
        </div>
        <!--section4-->
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <p class="h4 mt-4">Requirements</p>
                    <p class="h6 font-weight-normal mt-3"><i class="fas fa-circle mr-2 icon2"></i>Modern javascript</p>
                    <p class="h6 font-weight-normal"><i class="fas fa-circle mr-2 icon2"></i>Basic programming principles</p>
                    <p class="h6 font-weight-normal"><i class="fas fa-circle mr-2 icon2"></i>Basic knowledge of node-help</p>

                    <p class="h4 mt-5 pt-3">Description</p>
                    <p class="h6 font-weight-normal mt-2">This is a project based course where we build an extensive, in depth backend API foe DevCamper, a bootcamp directory app. we will start from scratch and end up with professional deployed API with documentation.
                    </p>
                    <p class="mt-4"><i class="fas fa-circle mr-2 icon2"></i>HTTP Essentials</p>
                    <p class="mt-4 mb-3"><i class="fas fa-circle mr-2 icon2"></i>Postman Client</p>
                </div>

            </div>
        </div>
@endsection