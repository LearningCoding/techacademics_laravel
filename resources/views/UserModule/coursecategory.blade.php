<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Categories</title>
    <link rel="stylesheet" href="{{asset('css/bootstrap.css')}}">
    <script src="{{asset('jquery.js')}}"></script>
    <script src="{{asset('js/bootstrap.js')}}"></script>
    <link rel="stylesheet" href="{{asset('fontawesome/css/all.css')}}">
    <link rel="stylesheet" href="{{asset('coursecategory.css')}}">
    <style>
        .main2 button {
            display: block;
        }
        
        .main2 h5,
        .main2 button {
            text-align: center;
        }
    </style>
</head>

<body class="bg-light mx-auto">
    <!--Menu Bar  -->
    <div class="menu">
        <nav class="navbar navbar-expand-lg navbar-light" id="menubar">
            <a href="mainpage.html" class="navbar-brand text-white">Techacademics</a>
            <button class="navbar-toggler" data-toggle="collapse" data-target="#hit">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="hit">
                <ul class="navbar-nav mx-auto">
                    <li class="nav-item">
                        <a href="mainpage.html" class="nav-link">Home</a>
                    </li>
                    <li class="nav-item">
                        <div class="search-bar">
                            <form action="">
                                <input type="text">
                                <i class="fa fa-search search-icon"></i>
                            </form>
                        </div>
                    </li>
                    <li class="nav-item dropdown">
                        <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">Courses</a>
                        <div class="dropdown-menu">
                            <a href="coursecategory.html" class="dropdown-item">IT</a>
                            <a href="#" class="dropdown-item">Science</a>
                            <a href="#" class="dropdown-item">Marketing</a>
                            <a href="#" class="dropdown-item">Health</a>
                            <a href="#" class="dropdown-item">Buisness</a>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a href="instructor.html" class="nav-link">Tutors</a>
                    </li>
                    <li class="nav-item">
                        <a href="viewmycourses.html" class="nav-link">My Courses</a>
                    </li>
                    <li class="nav-item">
                        <a href="cart_page.html" class="nav-link ">Cart</a>
                    </li>
                </ul>
                <ul class="navbar-nav ml-auto">
                    <div class="dropdown">
                        <a class="dropdown-toggle" href="#" data-toggle="dropdown">
                          My Profile
                        </a>
                        <div class="dropdown-menu profile_dropdown">
                            <a class="dropdown-item" href="viewmycourses.html">My Courses</a>
                            <a class="dropdown-item" href="cart_page.html">My Cart</a>
                            <a class="dropdown-item" href="profile.html">Edit Profile</a>
                            <a class="dropdown-item" href="changepassword.html">Change Password</a>
                            <a class="dropdown-item" href="#">Logout</a>
                        </div>
                    </div>
                </ul>
                <!-- <ul class="navbar-nav ml-auto login ">
                    <li class="nav-item ">
                        <a href="# " class="nav-link ">Login</a>
                    </li>
                    <li class="nav-item ">
                        <a href="# " class="nav-link ">Sign Up</a>
                    </li>
                </ul> -->
            </div>
        </nav>
    </div>
    <!-- Decription of content -->
    <div class="container mt-5">
        <div class="row">
            <div class="col-md-12 main" style="box-shadow: 5px 5px 5px black;">
                <h2>IT Courses</h2>
                <p>Etrain range of free online IT training courses includes clear and simple lessons on how to develop software, manage computer networks, and maintain vital IT systems across computers and phones. In today's digital world, these pieces of
                    technology facilitate almost everything we do in our personal and professional lives. This means that IT professionals are some of the most valued employees today.By studying up, you can join their ranks.</p>
            </div>
        </div>
    </div>
    <!-- Info of courses -->
    <div class="container-fluid">
        <div class="row main2">
            <div class="col-md-12">
                <h2>Top 20 IT Courses</h2>
            </div>
            <div class="col-sm-6 col-md-6 col-lg-4">
                <img src="../Images/p.jpg" class="d-block mx-auto">
                <h5>Introduction of Information Technology</h5>
                <button class="btn btn-outline-dark">Start Now</button>
            </div>
            <div class="col-sm-6 col-md-6 col-lg-4">
                <img src="../Images/p6.jpg" class="d-block mx-auto">
                <h5>Introduction of Information Technology</h5>
                <button class="btn btn-outline-dark">Start Now</button>
            </div>
            <div class="col-sm-6 col-md-6 col-lg-4">
                <img src="../Images/p5.jpg" class="d-block mx-auto">
                <h5>Introduction of Information Technology</h5>
                <button class="btn btn-outline-dark">Start Now</button>
            </div>
            <div class="col-sm-6 col-md-6 col-lg-4">
                <img src="../Images/p1.jpg" class="d-block mx-auto">
                <h5>Introduction of Information Technology</h5>
                <button class="btn btn-outline-dark">Start Now</button>
            </div>
            <div class="col-sm-6 col-md-6 col-lg-4">
                <img src="../Images/p2.jpg" class="d-block mx-auto">
                <h5>Introduction of Information Technology</h5>
                <button class="btn btn-outline-dark">Start Now</button>
            </div>
            <div class="col-sm-6 col-md-6 col-lg-4">
                <img src="../Images/p4.jpg" class="d-block mx-auto">
                <h5>Introduction of Information Technology</h5>
                <button class="btn btn-outline-dark">Start Now</button>
            </div>
            <div class="col-sm-6 col-md-6 col-lg-4">
                <img src="../Images/p3.jpg" class="d-block mx-auto">
                <h5>Introduction of Information Technology</h5>
                <button class="btn btn-outline-dark">Start Now</button>
            </div>
            <div class="col-sm-6 col-md-6 col-lg-4">
                <img src="../Images/p.jpg" class="d-block mx-auto">
                <h5>Introduction of Information Technology</h5>
                <button class="btn btn-outline-dark">Start Now</button>
            </div>
            <div class="col-sm-6 col-md-6 col-lg-4">
                <img src="../Images/p.jpg" class="d-block mx-auto">
                <h5>Introduction of Information Technology</h5>
                <button class="btn btn-outline-dark">Start Now</button>
            </div>
            <div class="col-sm-6 col-md-6 col-lg-4">
                <img src="../Images/p3.jpg" class="d-block mx-auto">
                <h5>Introduction of Information Technology</h5>
                <button class="btn btn-outline-dark">Start Now</button>
            </div>
            <div class="col-sm-6 col-md-6 col-lg-4">
                <img src="../Images/p6.jpg" class="d-block mx-auto">
                <h5>Introduction of Information Technology</h5>
                <button class="btn btn-outline-dark">Start Now</button>
            </div>
            <div class="col-sm-6 col-md-6 col-lg-4">
                <img src="../Images/p.jpg" class="d-block mx-auto">
                <h5>Introduction of Information Technology</h5>
                <button class="btn btn-outline-dark">Start Now</button>
            </div>
            <div class="col-sm-6 col-md-6 col-lg-4">
                <img src="../Images/p2.jpg" class="d-block mx-auto">
                <h5>Introduction of Information Technology</h5>
                <button class="btn btn-outline-dark">Start Now</button>
            </div>
            <div class="col-sm-6 col-md-6 col-lg-4">
                <img src="Images/p5.jpg " class="d-block mx-auto">
                <h5>Introduction of Information Technology</h5>
                <button class="btn btn-outline-dark ">Start Now</button>
            </div>
            <div class="col-sm-6 col-md-6 col-lg-4">
                <img src="../Images/p4.jpg" class="d-block mx-auto">
                <h5>Introduction of Information Technology</h5>
                <button class="btn btn-outline-dark">Start Now</button>
            </div>
            <div class="col-sm-6 col-md-6 col-lg-4">
                <img src="../Images/p2.jpg" class="d-block mx-auto">
                <h5>Introduction of Information Technology</h5>
                <button class="btn btn-outline-dark">Start Now</button>
            </div>
        </div>
    </div>
    <footer class="footer container-fluid">
        <div class="upper-footer">
            <div class="row">
                <div class="col-md-3 mx-auto mt-4">
                    <p>Our mission is to provide a free, world-class education to anyone,anywhere.</p>
                    <!-- <img src="bby.jpg " alt=" " class="img-fluid d-block img mb-2 "> -->

                </div>
                <div class="col-md-3 mx-auto mt-4">
                    <p class="font-weight-bold">ABOUT</p>
                    <p><a>News</a></p>
                    <p><a>Impact</a></p>
                    <p><a>Our team</a></p>
                    <p><a>Our leadership</a></p>
                    <p><a>Careers</a></p>
                    <p><a>Internship</a></p>
                </div>
                <div class="col-md-3 mt-4">
                    <p class="font-weight-bold">CONTACT US</p>
                    <p><span class="font-weight-bold">Address :</span> Hath of it fly signs bear be one blessed after</p>
                    <p><a>Help center</a></p>
                    <p><a>Share your story</a></p>

                    <p><a><span class="font-weight-bold">Phone :</span> 600540987</a></p>
                    <p><a><span class="font-weight-bold">Email :</span>info@learning.com </a></p>
                </div>
            </div>
        </div>
        <hr>
        <div class="icons">
            <div class="row">
                <div class="col-md-6 list mx-auto">
                    <ul>
                        <li>Terms of use</li>
                        <li class="ml-5">Privacy Policy</li>
                    </ul>
                </div>
                <div class="col-md-6">
                    <i class="fab fa-facebook"></i>
                    <i class="fab fa-twitter"></i>
                    <i class="fab fa-instagram"></i>
                </div>
            </div>
        </div>
        <hr>
        <div class="lower-footer text-center">
            Copyright @E-Learning All rights reserved
        </div>
    </footer>
</body>

</html>