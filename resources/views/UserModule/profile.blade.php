<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Student Profile</title>
    <link rel="stylesheet" href="{{asset('css/bootstrap.css')}}">
    <script src="{{asset('jquery-3.4.1.js')}}"></script>
    <script src="{{asset('js/bootstrap.js')}}"></script>
    <link rel="stylesheet" href="{{asset('fontawesome/css/all.css')}}">
    <link rel="stylesheet" href="{{asset('user_css/profile.css')}}">

</head>
<body style="background-color:#f2f3f5;">
    <div class="container-fluid">
        <div class="row">
            <!-- first section-->
            <div class="col-md-12 mx-auto text-center" id="first-section">
                <h2 class="bg-info text-white">Public profile</h2>
                <p>Add information about yourself</p>
            </div>
            <!-- second section for data input(second div)-->
            <div class="col-md-8 mx-auto" id="second-section">
                <div class="second-inner">
                <form action="">
                <label for="email"><h5>My Email id -:</h5></label>
                <p class="form-control email">teach@gmail.com</p>
                <label for="name"><h5>Name -:</h5></label>
                <p class="form-control name">raghav</p>
                <label for="clgname"><h5>College Name -:</h5></label>
                <input type="text" class="form-control clgname">
                <label for="gender" class="mt-2"><h5>Gender </h5></label><br>
                <input type="radio" name="gender" id="" class="gender">Male
                <input type="radio" name="gender" id="" class="gender">Female <br>
                <label for="" class="mt-2"><h5>Links:</h5></label>
                <input type="text" class="form-control" placeholder="Website(http(s)://..)"><br>
                <input type="text" class="form-control" placeholder="Twitter Profile">
                <p id="para">Add your twitter username(e.g.johnsmith).</p>
                <input type="text" class="form-control" placeholder="Facebook Profile">
                <p id="para">Input your Facebook username(e.g.johnsmith).</p>
                <input type="text" class="form-control" placeholder="Linkedln Profile">
                <p id="para">Input your Linkedln resourse id(e.g.in/johnsmith).</p>
                <input type="text" class="form-control" placeholder="Youtube Profile">
                <p id="para">Input your Facebook username(e.g.johnsmith).</p>
                <button class="btn btn-info mx-auto">Save</button>
                </form>
                </div>
            </div>
        </div>
    </div>
    
</body>
</html>