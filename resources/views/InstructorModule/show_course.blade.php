<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Created Courses</title>
    <link rel="stylesheet" href="{{asset('css/bootstrap.css')}}">
    <script src="{{asset('jquery-3.4.1.js')}}"></script>
    <script src="{{asset('js/bootstrap.js')}}"></script>
    <style>
        .container-fluid
        {
            padding-left: unset !important;
            padding-right: unset !important;
        }
    </style>
</head>
<body style="background-color: #f2f3f5;">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 mx-auto">
                <h1 class="text-center bg-info text-white">Created Courses</h1>
            </div>
            <div class="col-md-8 mx-auto mt-4">
                <label for="">Title</label>
                <p class="form-control">Php</p>
                <label for="">Category</label>
                <p class="form-control">IT</p>
                <label for="">What students will learn</label>
                <p class="form-control">Html , Css</p>
                <label for="">Course requirement</label>
                <p class="form-control">Basic of programming</p>
                <label for="">Course content</label>
                <p class="form-control">Php , laravel</p>
                <label for="">Description</label>
                <p class="form-control">Php full stack course</p>
                <label for="">Price</label>
                <p class="form-control">500</p>
                <input type="button" value="Edit/Manage" class="btn btn-info mt-3">
            </div>
        </div>
    </div>
</body>
</html>