<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Instructor Profile</title>
    <link rel="stylesheet" href="{{asset('css/bootstrap.css')}}">
    <script src="{{asset('jquery-3.4.1.js')}}"></script>
    <script src="{{asset('js/bootstrap.js')}}"></script>
    <link rel="stylesheet" href="{{asset('fontawesome/css/all.css')}}">
    <link rel="stylesheet" href="{{asset('instructor_css/instructorprofile.css')}}">
</head>
<body>
    <div class="container mt-5">
        <h2>Profile & Settings</h2>
        <!-- Nav tabs -->
        <ul class="nav nav-tabs mt-4" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" data-toggle="tab" href="#home">Profile details</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#menu1">Profile picture</a>
            </li>
        </ul>
    
        <!-- Tab panes -->
        <div class="tab-content">
            <!-- first profile page -->
            <div id="home" class="container tab-pane active">
                <div class="row">
                    <div class="col-md-6 mt-4 input-field">
                    <form action="">
                        <label for="">Full Name</label>
                        <p class="form-control">Jarvis</p>
                        <label for="">Email Id</label>
                        <p class="form-control">ins@gmail.com</p>
                        <label for="">Educational Details</label>
                        <input type="text" placeholder="'Engineer at Udemy' or 'Architect'" class='form-control'>
                        <label for="">Biography</label>
                        <textarea class="form-control " name="message" cols="50" rows="5" placeholder="B  I"></textarea>
                        <p>Your biography should have at least 50 characters, links and coupon codes are not permitted.</p>
                        <label for="">Language</label>
                        <input type="text" placeholder="eg. Hindi,English" class='form-control'>
                    </form>
                    </div>
                    <div class="col-md-6 mt-4">
                        <form action="">
                            <label for="">Website</label>
                            <input type="text" placeholder="Url" class="form-control">
                            <label for="">Twitter</label><br>
                            <input type="text" class="twit" placeholder="http://www.twitter.com/" disabled ><input type="text" class="twit" placeholder="Username">
                            <label for="">Facebook</label><br>
                            <input type="text" class="twit" placeholder="http://www.facebook.com/" disabled><input type="text" class="twit" placeholder="Username">
                            <label for="">LinkedIn</label><br>
                            <input type="text" class="twit" placeholder="http://www.linkedin.com/" disabled><input type="text" class="twit" placeholder="Resource ID">
                            <label for="">Youtube</label><br>
                            <input type="text" class="twit" placeholder="http://www.youtube.com/" disabled><input type="text" class="twit" placeholder="Username">
                        </form>
                    </div>
                    <div class="col-md-12 mt-4" style="margin-left: 1100px ;">
                        <a href="" class="btn">Save</a>
                    </div>
                </div>
            </div>

            <!-- second profile page -->

            <div id="menu1" class="container tab-pane fade"><br>
                <div class="col-md-12 mt-5">
                    <h6>Image Preview</h6>
                    <p>Minimum 200x200 pixels, Maximum 6000x6000 pixels</p>
                    <input type="file">
                </div>
                <div class="col-md-12 mt-4" style="margin-left: 340px ;">
                    <a href="" class="btn btn-info">Save</a>
                </div>
            </div>
        </div>
    </div>
</body>
</html>